import { NgModule, Component } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { StudentComponent } from './student/student.component';
import { StudentdetailsComponent } from './studentdetails/studentdetails.component';
import { PagenotfoundComponent } from './pagenotfound/pagenotfound.component';
import { NameEditorComponent } from './name-editor/name-editor.component';



const routes: Routes = [
  {
    path: '',redirectTo:'student', pathMatch:'full' 
  },
  {
    path: 'student',
    children:[
        {
          path:'',component:StudentComponent
        },
        {
          path: 'studentdetails',component:StudentdetailsComponent
        }
    ]  
  },
  {
    path: 'studentdetails',component:StudentdetailsComponent
  },
  {
    path: 'reactive-form',component:NameEditorComponent
  },
  
  {
    path: '**',component:PagenotfoundComponent
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
