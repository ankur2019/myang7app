import { Component, OnInit } from '@angular/core';

import { FormGroup, FormControl,FormBuilder, Validators } from '@angular/forms';
import { FormArray } from '@angular/forms';

@Component({
  selector: 'app-name-editor',
  templateUrl: './name-editor.component.html',
  styleUrls: ['./name-editor.component.css']
})
export class NameEditorComponent implements OnInit {

  // profileForm = new FormGroup({
  //   firstName: new FormControl(''),
  //   lastName: new FormControl(''),
  //   address: new FormGroup({
  //     street: new FormControl(''),
  //     city: new FormControl(''),
  //     state: new FormControl(''),
  //     zip: new FormControl('')
  //   })
  // });

  profileForm = this.fb.group({
    firstName: ['', Validators.required],
    lastName:['',[Validators.required, Validators.minLength(5)]],
    email:['', Validators.required],
    address: this.fb.group({
      street: [''],
      city: [''],
      state: [''],
      zip: ['']
    }),
  });

  

  // aliases: this.fb.array([
  //   this.fb.control('')
  // ]);

  constructor(private fb: FormBuilder) { }

  ngOnInit() {
    this.profileForm.get("firstName").valueChanges.subscribe(x => {
      console.log('firstname value changed')
      console.log(x)
    })
  }

  onSubmit() {
    // TODO: Use EventEmitter with form value
    console.warn(this.profileForm.get('firstName').value);
  }

  onReset() {
    // TODO: Use EventEmitter with form value
    this.profileForm.reset();
  }

}
