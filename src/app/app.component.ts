import { Component, HostListener, Inject } from '@angular/core';
import { MyserviceService } from './myservice.service';
import { Router } from '@angular/router';
import { NewserviceService } from './newservice.service';
import { FormControl } from '@angular/forms';
import { FormGroup } from '@angular/forms';
import { FormBuilder } from '@angular/forms';
import { NgForm } from '@angular/forms';
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
  preserveWhitespaces: true,
  providers: [NewserviceService]
})
export class AppComponent {
  title = 'Saho Shop';

  students:any[]=[];

  // signUpForm: FormGroup;
  FirstName: string="";
  LirstName: string="";
  Email: string="";
  Password: string="";

  signUpForm = new FormGroup({
        fname: new FormControl(),
        lnane: new FormControl(),
        // EmailId: new FormControl(),
        // userPassword: new FormControl(),
    }); 

  dataName:string = "NotGiven";
  dataName1:string = "Rahul";
  hotel:any = [];
  serviceTest:string;
  

  dob = new Date(1986,2,11);
  


  constructor(@Inject(MyserviceService) myserviceService,private router:Router, private frmBuilder:FormBuilder,private _newService:NewserviceService) 
  {

      // this.signUpForm = frmBuilder.group({
      //     fname: new FormControl(),
      //     lnane: new FormControl(),
      //     // EmailId: new FormControl(),
      //     // userPassword: new FormControl(),
      // }); 
      console.log(myserviceService);
      console.log("Hi This is component");
      this.students=[
        {
           'id': 1,
           'name': "Ankur",
           'email': "a@gmail.com",
           'gender': "male",
           'salary': 20000
        },
        {
          'id': 2,
          'name': "Bnkur",
          'email': "b@gmail.com",
          'gender': "male",
          'salary': 30000
        },
        {
          'id': 3,
          'name': "Cnkur",
          'email': "c@gmail.com",
          'gender': "female",
          'salary': 40000
        }
      ];
  }

  colsForStu:number = 8;
  
  getMoreStudent():void{
    this.students=[
      {
         'id': 1,
         'name': "Ankur",
         'email': "a@gmail.com",
         'gender': "male"
      },
      {
        'id': 2,
        'name': "Bnkur",
        'email': "b@gmail.com",
        'gender': "male"
      },
      {
        'id': 3,
        'name': "Cnkur",
        'email': "c@gmail.com",
        'gender': "female"
      },
      {
        'id': 4,
        'name': "Tnkur",
        'email': "e@gmail.com",
        'gender': "male"
      }
    ];
  }

  trackByStudentId(index: number, student:any): string {
      return student.id
  }

  checkNgIf = false;
  btnclick()
  {
      alert("you clicked me");
  }

  isValid:boolean = true;  
  changeValue (valid) {
    this.isValid = valid;  
  }

  public choose = '';
  setValue(drp:any){
     this.choose = drp.target.value;
  }

  //=====================================================
  countryDetails:any = [
     {
         'country': 'India',
         'people': [
           { 
               'name': 'in1'
           },
           { 
               'name': 'in2'
           },
           { 
               'name': 'in3'
           }   
         ],
     },
     {
        'country': 'uk',
        'people': [
          { 
              'name': 'uk1'
          },
          { 
              'name': 'uk2'
          },
          { 
              'name': 'uk3'
          }   
        ],
     }
  ];

  //=========================================================================

  getClass() {
      let classCss;
      
      classCss = {
          'one': true,
          'two': true
      }

      return classCss;
  }

  //==========================================================================
  showStudent() {
      
      this.router.navigate(['/student']);
  }

  reactiveForm() {    
    this.router.navigate(['/reactive-form']);
  }
  //=========================================================================
  // For Service Test
  ngOnInit(){
     this.hotel = this._newService.hotel;
     this.serviceTest = this._newService.display();
     console.log("hotel==>"+this.hotel);
     console.log("hotel==>"+this.serviceTest);
  }

  // for template form  
  Register(ngForm:any) {
      // 
      console.log(ngForm);
      debugger;
      var firstName = ngForm.controls.firstName.value;
      var lastName = ngForm.controls.lastName.value;
      var email = ngForm.controls.email.value;
      console.log("first",firstName);

  }

  // for Reactive Form
  postData(signUpForm: NgForm) {
      console.log(signUpForm.controls)
  }

  

  // @HostListener(`click`,[`$event`])
  // onhostclick(event:Event)
  // {
  //     alert("hello");
  // }
}
